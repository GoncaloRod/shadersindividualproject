Shader "Custom/World"
{
    Properties
    {
        [NoScaleOffset]_DayMap ("Day Time Map", 2D) = "white" {}
        [NoScaleOffset]_NightMap ("Night Time Map", 2D) = "white" {}
        
        [Space(20)]
        
        _LightsPower ("Lights Power", Range(1, 3)) = 1 
        
        [Space(20)]
        
        [Toggle] _Void ("Void", Int) = 0
        
        [Space(20)]
        
        [Toggle] _PickleRick ("Pickle Rick", Int) = 0
        [NoScaleOffset] _PickleRickMap ("Pickle Rick Map", 2D) = "white" {}
        
        [Space(20)]
        
        [Toggle] _Waves ("Waves", Int) = 0
        [Toggle] _WavesMovement ("Waves Movment", Int) = 0
        [NoScaleOffset] _WavesMap ("Waves Map", 2D) = "white" {}
        [NoScaleOffset] _WavesNoiseMap ("Waves Noise Map", 2D) = "white" {}
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Transparent"
        }
        LOD 200
        
        Cull Front
    
        CGPROGRAM
        #pragma surface surf Lambert fullforwardshadows alpha

        #pragma target 3.0

        struct Input
        {
            float2 uv_PickleRickMap;
        };

        int _PickleRick;
        sampler2D _PickleRickMap;

        void surf (Input IN, inout SurfaceOutput o)
        {
            if (_PickleRick)
            {
                o.Albedo = tex2D(_PickleRickMap, IN.uv_PickleRickMap).rgb;
                o.Alpha = tex2D(_PickleRickMap, IN.uv_PickleRickMap).a;
            }
        }
        ENDCG
        
        Cull Back
        
        CGPROGRAM
        #pragma surface surf Lambert fullforwardshadows alpha

        #pragma target 3.0

        struct Input
        {
            float2 uv_DayMap;
            float2 uv_NightMap;
            float2 uv_WavesMap;
            float2 uv_WavesNoiseMap;
        };

        sampler2D _DayMap;
        sampler2D _NightMap;

        float _LightsPower;

        int _Void;

        int _Waves;
        int _WavesMovement;
        sampler2D _WavesMap;
        sampler2D _WavesNoiseMap;

        void surf (Input IN, inout SurfaceOutput o)
        {
            float NdotL = dot(o.Normal, _WorldSpaceLightPos0);

            fixed4 color;

            o.Alpha = 1;
            
            if (NdotL > 0)
            {
                color = tex2D(_DayMap, IN.uv_DayMap);

                if (_Void)
                {
                    if (color.b > color.r && color.b > color.g)
                    {
                        o.Alpha = 0;
                    }
                }
                else if (_Waves)
                {
                    if (color.b > color.r && color.b > color.g)
                    {
                        float2 uv = IN.uv_WavesMap;

                        if (_WavesMovement)
                        {
                            float noise = tex2D(_WavesNoiseMap, IN.uv_WavesNoiseMap).x;
                            
                            uv = float2(sin(_Time.x + uv.x * noise), sin(_Time.x + uv.y * noise));
                        }
                        
                        color = tex2D(_WavesMap, uv);
                    }
                }
            }
            else
            {
                color = tex2D(_NightMap, IN.uv_NightMap);

                float avgr = (color.r + color.g + color.b) / 3;

                if (avgr > 0.25)
                    color += color * (color * _LightsPower);
            }
            
            o.Albedo = color.rgb;
        }
        ENDCG
    }
    
    FallBack "Diffuse"
}
